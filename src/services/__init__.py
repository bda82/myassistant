import logging
import datetime as dt
from abc import ABC

from sqlalchemy.exc import IntegrityError


class ServiceBase(ABC):
    model = None
    _logger = logging.getLogger(__name__)

    def __init__(self, session):
        self.session = session
        self.name = self.model.__name__

    def mutate(self, **kwargs) -> dict:
        return kwargs

    def save(self, obj):
        with self.session.begin():
            self.session.add(obj)
            try:
                self.session.commit()
            except IntegrityError as e:
                self.session.rollback()
                self._logger.error(e)

    def create(self, **kwargs):
        data = self.mutate(**kwargs)
        obj = self.model(**data)
        self.save(obj)
        self.session.refresh(obj)
        self._logger.info("Created %s:%s", self.name, obj.id)
        return obj

    def get(self, pk):
        return self.session.query(self.model).get(pk)

    def delete(self, pk):
        obj = self.get(pk)
        self.session.delete(obj)
        try:
            self.session.commit()
        except IntegrityError as e:
            self.session.rollback()
            msg = f"Cannot delete {self.name}:{pk}"
            self._logger.warning(f"{msg} Original:{e}")
            raise Exception(msg)

        self._logger.info("Deleted %s:%s", self.name, pk)

        return obj

    def update(self, pk, ignore_unset: bool = False, **kwargs):
        obj = self.get(pk)

        if hasattr(obj, "updated_at"):
            obj.updated_at = dt.datetime.utcnow()

        data = self.mutate(**kwargs)

        for field, value in data.items():
            if value is not None or not ignore_unset:
                setattr(obj, field, value)

        self.save(obj)

        self.session.refresh(obj)
        return obj

    def get_list(self):
        models = []

        with self.session.begin():
            q = self.session.query(self.model)
            models = q.all()

        return models
