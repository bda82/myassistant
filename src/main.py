from gunicorn.app.base import BaseApplication
from gunicorn.workers.sync import SyncWorker

from app import MyAssistant
from config import AppConfig


class CustomWorker(SyncWorker):
    def handle_quit(self, sig, frame):
        self.app.application.stop(sig)
        super(CustomWorker, self).handle_quit(sig, frame)

    def run(self):
        self.app.application.start()
        super(CustomWorker, self).run()


class Application(BaseApplication):
    def __init__(self, app, options=None):
        self.options = options or {}
        self.application = app

        super(Application, self).__init__()

    def load_config(self):
        for key, value in self.options.items():
            self.cfg.set(key.lower(), value)

        self.cfg.set('worker_class', 'main.CustomWorker')

    def load(self):
        return self.application


def main():

    cfg = AppConfig()

    api_app = MyAssistant(cfg)
    gunicorn_app = Application(api_app, cfg.gunicorn)

    gunicorn_app.run()


main()
