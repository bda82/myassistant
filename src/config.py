

class DatabaseConfig:
    connection = "mysql+mysqlconnector://assistant:password@localhost:3306/assistant"


class AppConfig:
    def __init__(self):
        self.db = DatabaseConfig()
        self.gunicorn = {
            "bind": "0.0.0.0:8000",
            "workers": 1,
            "timeout": 30
        }
