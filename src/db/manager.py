import sqlalchemy
import logging
from sqlalchemy import orm
from sqlalchemy.orm import scoping
from sqlalchemy.ext.declarative import declarative_base

logger = logging.getLogger(__name__)

SAModel = declarative_base()


class DBManager:
    def __init__(self, connection=None):
        self.connection = connection
        self.engine = sqlalchemy.create_engine(self.connection)
        self.db_session = scoping.scoped_session(
            orm.sessionmaker(
                bind=self.engine,
                autocommit=True
            )
        )

    @property
    def session(self):
        return self.db_session

    def setup(self):
        try:
            SAModel.metadata.create_all(self.engine)
        except Exception as e:
            logger.error(f"{e}")
