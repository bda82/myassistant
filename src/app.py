import falcon
from db.manager import DBManager
from notes.resources import NotesResource


class MyAssistant(falcon.App):

    def __init__(self, cfg):
        super(MyAssistant, self).__init__()
        self.cfg = cfg
        self.manager = DBManager(self.cfg.db.connection)
        self.manager.setup()

        self.append_routes()

    def append_routes(self):
        self.add_route("/notes", NotesResource(self.manager))

    def start(self):
        pass

    def stop(self):
        pass
