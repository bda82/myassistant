import falcon

from notes.service import NoteService
from resources import BaseResource


class NotesResource(BaseResource):
    service: NoteService

    def __init__(self, manager):
        self.service = NoteService(manager.session)
        super().__init__(manager.session)

    def on_get(self, req, resp):
        model_list = self.service.get_list()
        notes = [model.as_dict for model in model_list]

        resp.status = falcon.HTTP_200
        resp.media = {
            "notes": notes
        }

    def on_post(self, req, resp):
        model = self.service.create(**req.media)

        resp.status = falcon.HTTP_201
        resp.media = {
            "id": model.id
        }
