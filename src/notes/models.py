import sqlalchemy as sa
from db.manager import SAModel


class Note(SAModel):
    __tablename__ = "note"

    id = sa.Column(sa.Integer, primary_key=True)
    title = sa.Column(sa.String(128), unique=True)
    description = sa.Column(sa.String(128), unique=True)
    body = sa.Column(sa.String(128), unique=True)

    @property
    def as_dict(self):
        return {
            "id": self.id,
            "title": self.title,
            "description": self.description,
            "body": self.body
        }
